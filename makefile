DEBUG=true
CC=gcc
ifeq ($(DEBUG),true)
	CFLAGS=-W -Wall -g
	LDFLAGS=
else
	CFLAGS=-W -Wall
	LDFLAGS=
endif

EXEC= station
SRC= $(wildcard *.c)
OBJ= $(SRC:.c=.o)

all: $(EXEC) isdebug run

isdebug: 
ifeq ($(DEBUG),true)
	@echo "Compilation avec debug"
else
	@echo "Génération en mode release"
endif

run: ${EXEC}
	@echo "Execution"
	./${EXEC}

#station
station: $(OBJ)
	@$(CC) -o $@ $^ $(LDFLAGS)

#default 
%.o: %.c
	@$(CC) -o $@ -c $< $(CFLAGS)

#delete
.DLT: clean mrproper

clean:
	@rm -rf *.o

mrproper: clean
	@rm -rf $(EXEC)