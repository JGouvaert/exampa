#ifndef H_STATION
#define H_STATION

//Important
#define MAX 20
#define STRSIZE 30

//Pour la lisibilité
#define PLEIN 0
#define VIDE 1

#define CROISSANT 0
#define DECROISSANT 1

//Station basics
typedef struct station station;

typedef struct station {
    char nom[STRSIZE];
    int nombre_velos;
    int max_velos;
    station* next;
} station;

void initstation(station * l);
station* createnewstation(station *p);
float taux_occupe(station* s);
int comparestation(station* s1, station* s2);
void permutestation(station* s1, station* s2);

//ContLIst basics
typedef struct contlist {
    station* head;
    int count;
    int max;
} contlist;

void initcont(contlist * l );

//Affiche la liste 
void printcont(contlist l);
void printsub(station* s, int max);

int scan_file(contlist* l, char fname[STRSIZE]);

//Tri la liste
void tri_croissant(contlist* l);
void tri_decroissant(contlist* l);
void station_tri(contlist *l, int type);

//Retourne le nombre de station et les affiches
//Attention, ces fonctions ré-organise la liste
int trop_pleines(contlist* l);
int trop_vides(contlist* l);
int station_trop(contlist *l, int type);

//Cherche dans la liste une station entrée par l'utilisateur
void recherche(contlist* l);

#endif
	