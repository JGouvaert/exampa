#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "station.h"

void initstation(station * l){
    l->nombre_velos = 0;
    l->max_velos = 0;
    l->next = NULL;
    return;
};

void stationaddname(station* s, char name[STRSIZE]){
    int len = strlen(name);
    if (len > 0 && name[len-1] == '\n'){
        name[len-1] = 0;
    }
    strncpy(s->nom, name, STRSIZE - 1);  
    s->nom[STRSIZE - 1] = '\0'; 
};

station* createnewstation(station *p){
    station* s;
    s = malloc(sizeof(station));
    initstation(s);
    if(p != NULL){
        p->next = s;
    }
    return s;
};

float taux_occupe(station* s){
    return (float)s->nombre_velos / (float)s->max_velos; 
}

int comparestation(station* s1, station* s2){
    float taux1 =  taux_occupe(s1);
    float taux2 =  taux_occupe(s2);
    if(taux1 == taux2){
        return 0;
    }else if(taux1 < taux2){
        return -1;
    }
    return 1;
}

void permutestation(station* s1, station* s2){
    station* tmp = malloc(sizeof(station));
    tmp->nombre_velos   = s2->nombre_velos;
    tmp->max_velos      = s2->max_velos;
    s2->nombre_velos    = s1->nombre_velos;
    s2->max_velos       = s1->max_velos;
    s1->nombre_velos    = tmp->nombre_velos;
    s1->max_velos       = tmp->max_velos;
    //ou 
    /*
    memcpy( tmp, s2, sizeof(station));
    memcpy( s2, s1, sizeof(station));
    memcpy( s1, tmp, sizeof(station));
    */
    free(tmp);
}

void initcont(contlist * l ){
    station * s;
    s = malloc(sizeof(station));
    initstation(s);

    l->head = s;
    l->count = 1;
    l->max = MAX;
    return;
};

void printsub(station* s, int max){
    if(s != NULL && max != 0){
        printf("%s: %i/%i\n", s->nom, s->nombre_velos, s->max_velos);
        printsub(s->next, max-1);
    }
    return;
};

void printcont(contlist l){
    printf("Max %i stations; Fichier contenant %i stations\n", l.max, l.count);
    printsub(l.head, l.count);
};

void tri_croissant(contlist* l){
    station* left;
    station* right = NULL;
    int echange;
    do{ 
        left = l->head;
        echange = 0;
        while(left->next != right){
            if (comparestation(left, left->next) == 1) 
			{
				permutestation(left, left->next); 
                echange = 1; 
			}
			left = left->next;
        }
        right = left;
    }while(echange);
};

void tri_decroissant(contlist* l){
    station* left;
    station* right = NULL;
    int echange;
    do{ 
        left = l->head;
        echange = 0;
        while(left->next != right){
            if (comparestation(left, left->next) == -1) 
			{
				permutestation(left, left->next); 
                echange = 1; 
			}
			left = left->next;
        }
        right = left;
    }while(echange);
};

static void (*tri(int ordre))(contlist* l)
{
    if (ordre % 2 == 0)
        return &tri_croissant;
    else
        return &tri_decroissant;
}

void station_tri(contlist *l, int type){
    void (*trifunc)(contlist*);
    trifunc = tri(type);
    return (*trifunc)(l);
}

int scan_file(contlist* l, char fname[STRSIZE])
{
    initcont(l);

    FILE *inputf;
    if ((inputf=fopen(fname,"r")) == NULL){
        printf("Erreur! Impossible d'ouvrir le fichier %s.\n", fname);
        return 1;
    }else{
        printf("Fichier %s ouvert, traitement en cours.\n", fname);
        char tmpch[STRSIZE];
        int count = 0;
        int mod = 0;

        station* tmp = createnewstation(NULL);
        l->head = tmp;
        while(fgets(tmpch, STRSIZE, inputf) != NULL){
            mod = count%3;
            if(mod == 0){
                if(count == 0){
                    stationaddname(tmp, tmpch);
                } else if(l->count <= l->max){
                    tmp = createnewstation(tmp);
                    stationaddname(tmp, tmpch);
                    l->count++;    
                }else{
                    printf("Mémoire pleine, ligne %i ignorée\n", count);
                    fclose(inputf);
                    printf("Fermeture du fichier\n");
                    return 0;
                }        
            }else if(mod == 1){
                tmp->nombre_velos = atoi(tmpch);
            }else{
                tmp->max_velos = atoi(tmpch);
            }
            count++;
            printf("Ligne %i traitée\n", count);
        }
    }
    fclose(inputf);
    printf("Fichier traité, fermeture du fichier\n\n");
    return 0;
}

int trop_pleines(contlist* l){
    station_tri(l, CROISSANT);

    station* s = l->head;
    int count = 0;
    while(s != NULL && taux_occupe(s) <= 0.75){
        count++;
        s = s->next;
    }
    printf("C'est station sont trop pleines : \n");
    int no = l->count;
    no -= count;
    printsub(s, no);
    return no;
}

int trop_vides(contlist* l){
    station_tri(l, DECROISSANT);

    station* s = l->head;
    int count = 0;
    while(s != NULL && taux_occupe(s) >= 0.25){
        count++;
        s = s->next;
    }
    printf("C'est station sont trop vides : \n");
    int no = l->count;
    no -= count;
    printsub(s, no);
    return no;
}

static int (*trop(int type))(contlist* l)
{
    if (type == VIDE)
        return &trop_vides;
    else //BY default trop plein
        return &trop_pleines;
}

int station_trop(contlist *l, int type){
    int (*tropfunc)(contlist*);
    tropfunc = trop(type);
    return (*tropfunc)(l);
}

void recherche(contlist* l){
    printf("\nDébut de la recherche de station \nNom de la station:\t");
    char nom[STRSIZE];
    scanf("%s",nom);

    printf("Recherche de %s; dans la liste contenant %i stations\n", nom, l->count);
    
    station* s = l->head;
    int found;
    while(s != NULL && (found = strcmp( s->nom, nom )) != 0){
        s = s->next;
    }
    if(found == 0){
        printf("Station trouvée : \n");
    }else{
        printf("Aucune station trouvée.\n");
    }
    printsub(s, 1);
    printf("Fin de la recherche de station \n\n");
}

int main()
{
    char fname[STRSIZE];
    printf("Bonjour, quel fichier voulez vous ouvrir (sans extension .txt)?\n");
    scanf("%123s",fname);
    strcat(fname,".txt");

    contlist l;
    
    if(!scan_file(&l, fname)){
        station_tri(&l, CROISSANT);
        printcont(l);
        recherche(&l);

        printf("%i stations sont trop pleines\n", station_trop(&l, PLEIN));
        printf("%i stations sont trop vides  \n", station_trop(&l, VIDE));
    }else{
        printf("Liste vide\n");
    }

    return 0;
}
